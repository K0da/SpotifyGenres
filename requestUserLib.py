import spotipy
import json
from collections import Counter

# loads the user token
f = open(".spotipyoauthcache", "r")
token = json.loads(f.read())["access_token"]
f.close()

sp = spotipy.Spotify(auth=token)


def get_latest_genres_listened(amount):
    genres = []
    latest_tracks: dict = sp.current_user_top_tracks(amount, 0, "short_term")
    for track in latest_tracks["items"]:
        for artists in track["artists"]:
            genres.extend(get_genres_from_artist(artists["id"]))
    # genres = list(set(genres))
    genres.sort()
    return genres


def find_genere_duplicates(list_of_genres: list) -> dict:
    counts = dict(Counter(list_of_genres))
    duplicates = {key: value for key, value in counts.items()}
    return duplicates


def get_genres_from_artist(artist_id) -> list:
    artist: dict = sp.artist(artist_id)
    return artist["genres"]


def saveInJson(data):
    f = open("data.json", "w")
    f.write(json.dumps(data))
    f.close()


def search_artist(search_term) -> dict:
    return sp.search("admo", type="artist")


def main():
    data = get_latest_genres_listened(30)
    data = find_genere_duplicates(data)
    most_common = dict(Counter(data).most_common(15))
    saveInJson(most_common)


if __name__ == "__main__":
    main()
